import React from "react";
import { View, StyleSheet, TouchableOpacity, Linking } from "react-native";
import { Header, Avatar, Text } from "react-native-elements";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useSelector } from "react-redux";

const About = () => {
  const AboutReducer = useSelector((state) => state.AboutReducer);

  return (
    <View style={styles.container}>
      <Header
        backgroundColor={"grey"}
        centerComponent={{ text: "About Me", style: { color: "#fff" } }}
      />
      <View style={{ alignItems: "center", marginTop: 30 }}>
        <Avatar
          rounded
          size="xlarge"
          source={{
            uri:
              "https://d17ivq9b7rppb3.cloudfront.net/small/avatar/20200506105706b1f6ba2e8a1b344f7a9dc42b3c5965b5.png",
          }}
        />
        <Text h4 style={{ marginTop: 20, color: "#000000" }}>
          {AboutReducer.name}
        </Text>
        <Text h5 style={{ color: "#0C0C0C", marginBottom: 10 }}>
          {AboutReducer.location}
        </Text>
      </View>

      <View>
        <Text
          style={{
            fontSize: 20,
            marginTop: 5,
            color: "#000000",
            fontStyle: "italic",
            marginLeft: 15,
          }}
        >
          Tentang Saya
        </Text>
        <Text
          h5
          style={{
            marginTop: 15,
            marginLeft: 15,
            marginRight: 10,
            color: "#000000",
          }}
        >
          {AboutReducer.AboutText}
        </Text>
      </View>

      <View style={{ alignItems: "center" }}>
        <Text
          style={{
            fontSize: 20,
            marginTop: 180,

            color: "#000000",
            fontStyle: "italic",
          }}
        >
          Contact Me
        </Text>
      </View>

      <View
        style={{
          marginTop: 20,
          justifyContent: "space-evenly",
          flexDirection: "row",
        }}
      >
        <TouchableOpacity
          onPress={() => Linking.openURL(AboutReducer.Facebook)}
        >
          <Icon name="facebook" size={40} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => Linking.openURL(AboutReducer.Instagram)}
        >
          <Icon name="instagram" size={40} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => Linking.openURL(AboutReducer.Whatsapp)}
        >
          <Icon name="whatsapp" size={40} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
});

export default About;
