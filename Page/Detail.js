import React from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Text,
} from "react-native";
import { Header, Avatar } from "react-native-elements";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useSelector } from "react-redux";
import { ScrollView } from "react-native-gesture-handler";

const Detail = (props) => {
  const AboutReducer = useSelector((state) => state.AboutReducer);
  const {
    title,
    type,
    url,
    company,
    company_url,
    location,
    description,
    company_logo,
  } = props.route.params.item;

  return (
    <ScrollView>
      <View style={styles.container}>
        <Header
          backgroundColor={"grey"}
          centerComponent={{ text: "Detail Job", style: { color: "#fff" } }}
        />
        <View style={{ alignItems: "center", marginTop: 30 }}>
          <Avatar
            rounded
            size="xlarge"
            source={{
              uri: company_logo,
            }}
          />
          <Text
            style={{
              marginTop: 20,
              color: "#000000",
              fontWeight: "bold",
              fontSize: 15,
            }}
          >
            {title}
          </Text>
          <Text style={{ color: "#0C0C0C", marginBottom: 10 }}>
            {`${company} - ${location}`}
          </Text>
        </View>

        <View>
          <Text
            style={{
              fontSize: 20,
              marginTop: 5,
              color: "#000000",
              fontStyle: "italic",
              marginLeft: 15,
            }}
          >
            Description
          </Text>
          <Text
            h5
            style={{
              marginTop: 15,
              marginLeft: 15,
              marginRight: 10,
              color: "#000000",
            }}
          >
            {description.replace(/<[^>]+>/g, "")}
          </Text>
        </View>

        <View style={{ alignItems: "center" }}>
          <Text
            style={{
              fontSize: 20,
              marginTop: 30,

              color: "#000000",
              fontStyle: "italic",
            }}
          >
            Interested ?
          </Text>
        </View>

        <View
          style={{
            marginTop: 10,
            justifyContent: "space-evenly",
            flexDirection: "row",
            marginBottom: 10,
          }}
        >
          <TouchableOpacity
            style={{
              width: 150,
              height: 50,
              backgroundColor: "grey",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => Linking.openURL(url)}
          >
            <Text style={{ color: "#FFFFFF" }}>Apply Now</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
});

export default Detail;
