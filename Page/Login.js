import React, { useState } from "react";
import Ilustrator from "../assets/Login.png";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { setForm } from "../Redux/action";

const Login = (props) => {
  const state = useSelector((state) => state.LoginReducer);
  const dispatch = useDispatch();

  const login = () => {
    const { email, password } = state;
    setTimeout(() => {
      props.navigation.navigate("Home", {
        email,
        password,
      });
    }, 500);
  };

  const onInputChange = (value, name) => {
    dispatch(setForm(name, value));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Login</Text>
      <Image style={styles.image} source={Ilustrator} />

      {
        //Input Container
      }
      <View style={styles.inputContainer}>
        <View>
          <TextInput
            style={styles.textInput}
            value={state.form.email}
            onChangeText={(value) => onInputChange(value, "email")}
            placeholder="Email"
          />
        </View>
      </View>
      <View>
        <TextInput
          style={styles.textInput}
          value={state.form.password}
          onChangeText={(value) => onInputChange(value, "password")}
          placeholder="Password"
        />
      </View>

      {
        //Button Container
      }
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.buttonLogin}>
          <Text style={styles.buttonText} onPress={() => login()}>
            NEXT
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonRegister}>
          <Text
            style={styles.textRegister}
            onPress={() => props.navigation.navigate("Register")}
          >
            REGISTER
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  titleText: {
    marginBottom: 28,
    marginRight: 280,
    color: "#000000",
    lineHeight: 27,
    margin: 20,
    fontFamily: "Comfortaa",
    fontSize: 24,
    textAlign: "left",
  },
  image: {
    marginLeft: 28,
    width: 348,
    height: 241,
  },
  inputContainer: {
    marginTop: 47,
    flexDirection: "row",
    marginBottom: 30,
  },
  textInput: {
    fontFamily: "Roboto",
    width: 343,
    fontSize: 20,
    paddingLeft: 20,
    height: 52,
    marginLeft: 28,
    borderRadius: 10,
    borderWidth: 2,
    borderStyle: "solid",
  },
  buttonContainer: {
    marginTop: 47,
    marginLeft: 28,
  },
  buttonLogin: {
    borderRadius: 6,
    width: 343,
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#000000",
  },
  buttonText: {
    fontFamily: "Roboto",
    fontSize: 20,

    color: "#FFFFFF",
  },
  buttonRegister: {
    borderRadius: 6,
    borderWidth: 2,
    borderStyle: "solid",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5F5F5",
    width: 343,
    height: 52,
    marginTop: 26,
  },
  textRegister: {
    fontFamily: "Roboto",
    fontSize: 20,
    color: "#000000",
  },
});

export default Login;
