import { combineReducers } from "redux";

const AboutState = {
  name: "Raditya Firman Syaputra",
  location: "Sragen , Jawa Tengah",
  AboutText:
    "Saya adalah Siswa SMA kelas 11 dari Sragen jurusan MIPA. Saat ini saya sedang Mendalami dunia pemrograman khususnya Web dan Mobile Development.",
  Facebook: "https://www.facebook.com/your.future.soon/",
  Instagram: "https://instagram.com/sekutumu",
  Whatsapp: "https://api.whatsapp.com/send?phone=6281232254875",
};

const RegisterState = {
  form: {
    name: "",
    email: "",
    password: "",
    repassword: "",
  },
};

const LoginState = {
  form: {
    email: "",
    password: "",
  },
  isWrongPass: false,
};

const RegisterReducer = (state = RegisterState, action) => {
  if ((action.type = "SET_FORM")) {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputType]: action.inputValue,
      },
    };
  }
  return state;
};

const AboutReducer = (state = AboutState, action) => {
  return state;
};

const LoginReducer = (state = LoginState, action) => {
  if ((action.type = "SET_FORM")) {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputType]: action.inputValue,
      },
    };
  }
  return state;
};

const reducer = combineReducers({
  RegisterReducer,
  AboutReducer,
  LoginReducer,
});

export default reducer;
